import Goban
import numpy as np
from random import choice

def evaluate(board, player):

    #print("Tableau transmis")
    #board.prettyPrint();
    
    score = 10000

    #partie jeu terminé
    if board.is_game_over():
        scoref = board._count_areas()
        score_b = board._nbBLACK + scoref[0]
        score_w = board._nbWHITE + scoref[1]
        if (player == board._BLACK and score_b > score_w): 
            return score
        elif (player == board._WHITE and score_b < score_w):
            return score
        elif (score_b == score_w):
            return score/2
        return -1* score
    
    opponent = board.flip(player)

    #calcul score Liberté
    scoreLiberty = 0
    indLib = 0
    zeroLibP = 0
    zeroLibO = 0
    for lib in board._stringLiberties:
        if lib != -1:
            if board[indLib] == player:
                if board._stringSizes[indLib] != 1:
                    scoreLiberty += 100*lib
                else:
                    scoreLiberty += 40*lib
                if lib == 1:
                    zeroLibP += 1
            elif board[indLib] == opponent:
                scoreLiberty -= 50*lib
                if lib == 1:
                    zeroLibO += 1
        indLib += 1
    if zeroLibP > 1:
        scoreLiberty = -scoreLiberty #ou return -score/2
    elif zeroLibO > 0:
        scoreLiberty = 2*scoreLiberty #ou return score/2


    #calcul score Union
    scoreUnion = 0
    indUni = 0
    for uni in board._stringUnionFind:
        if board[indUni] == player:
            scoreUnion += uni
        elif board[indUni] == opponent:
            scoreUnion -= uni
        indUni += 1

        
    #calcul score Edge
    scoreEdge = 0
    for edg in range(0,board._BOARDSIZE-1):
        if board[edg] == player:
            scoreEdge -= 20
        if board[edg*board._BOARDSIZE] == player:
            scoreEdge -= 20
        if board[edg*board._BOARDSIZE+board._BOARDSIZE] == player:
            scoreEdge -= 20
        if board[board._BOARDSIZE-edg] == player:
            scoreEdge -= 20
        if board[edg] == opponent:
            scoreEdge += 15
        if board[edg*board._BOARDSIZE] == opponent:
            scoreEdge += 15
        if board[edg*board._BOARDSIZE+board._BOARDSIZE] == opponent:
            scoreEdge += 15
        if board[board._BOARDSIZE-edg] == opponent:
            scoreEdge += 15


    #Euler number for eyes
    Q1P = 0
    Q1O = 0
    Q2P = 0
    Q2O = 0
    Q3P = 0
    Q3O = 0
    for ln in range(0,board._BOARDSIZE**2-1):
        #traitement 2x2 a partir de pion en bas a gauche
        if (ln < board._BOARDSIZE**2-board._BOARDSIZE-1 and ln%(board._BOARDSIZE-1) != 0):
            if (board[ln] == player and board[ln+1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE+1] == board._EMPTY):
                Q1P += 1
            if (board[ln] == opponent and board[ln+1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE+1] == board._EMPTY):
                Q1O += 1
            if (board[ln] == player and board[ln+1] == player and board[ln+board._BOARDSIZE] == player and board[ln+board._BOARDSIZE+1] == board._EMPTY):
                Q2P += 1
            if (board[ln] == opponent and board[ln+1] == opponent and board[ln+board._BOARDSIZE] == opponent and board[ln+board._BOARDSIZE+1] == board._EMPTY):
                Q2O += 1
            if (board[ln] == player and board[ln+1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE+1] == player):
                Q3P += 1
            if (board[ln] == opponent and board[ln+1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE+1] == opponent):
                Q3O += 1

        #traitement 2x2 a partir de pion en bas a droite
        if (ln < board._BOARDSIZE**2-board._BOARDSIZE and ln%board._BOARDSIZE != 0):
            if (board[ln] == player and board[ln-1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE-1] == board._EMPTY):
                Q1P += 1
            if (board[ln] == opponent and board[ln-1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE-1] == board._EMPTY):
                Q1O += 1
            if (board[ln] == player and board[ln-1] == player and board[ln+board._BOARDSIZE] == player and board[ln+board._BOARDSIZE-1] == board._EMPTY):
                Q2P += 1
            if (board[ln] == opponent and board[ln-1] == opponent and board[ln+board._BOARDSIZE] == opponent and board[ln+board._BOARDSIZE-1] == board._EMPTY):
                Q2O += 1
            if (board[ln] == player and board[ln-1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE-1] == player):
                Q3P += 1
            if (board[ln] == opponent and board[ln-1] == board._EMPTY and board[ln+board._BOARDSIZE] == board._EMPTY and board[ln+board._BOARDSIZE-1] == opponent):
                Q3O += 1

        #traitement 2x2 a partir de pion en haut a droite
        if (ln >= board._BOARDSIZE and ln%board._BOARDSIZE != 0):
            if (board[ln] == player and board[ln-1] == board._EMPTY and board[ln-board._BOARDSIZE] == board._EMPTY and board[ln-board._BOARDSIZE-1] == board._EMPTY):
                Q1P += 1
            if (board[ln] == opponent and board[ln-1] == board._EMPTY and board[ln-board._BOARDSIZE] == board._EMPTY and board[ln-board._BOARDSIZE-1] == board._EMPTY):
                Q1O += 1
            if (board[ln] == player and board[ln-1] == player and board[ln-board._BOARDSIZE] == player and board[ln-board._BOARDSIZE-1] == board._EMPTY):
                Q2P += 1
            if (board[ln] == opponent and board[ln-1] == opponent and board[ln-board._BOARDSIZE] == opponent and board[ln-board._BOARDSIZE-1] == board._EMPTY):
                Q2O += 1

        #traitement 2x2 a partir de pion en haut a gauche
        if (ln >= board._BOARDSIZE and ln%(board._BOARDSIZE-1) != 0):
            if (board[ln] == player and board[ln+1] == board._EMPTY and board[ln-board._BOARDSIZE] == board._EMPTY and board[ln-board._BOARDSIZE+1] == board._EMPTY):
                Q1P += 1
            if (board[ln] == opponent and board[ln+1] == board._EMPTY and board[ln-board._BOARDSIZE] == board._EMPTY and board[ln-board._BOARDSIZE+1] == board._EMPTY):
                Q1O += 1
            if (board[ln] == player and board[ln+1] == player and board[ln-board._BOARDSIZE] == player and board[ln-board._BOARDSIZE+1] == board._EMPTY):
                Q2P += 1
            if (board[ln] == opponent and board[ln+1] == opponent and board[ln-board._BOARDSIZE] == opponent and board[ln-board._BOARDSIZE+1] == board._EMPTY):
                Q2O += 1

        
    scoreEuler = ((Q1P-Q2P+2*Q3P)/4 - (Q1O-Q2O+2*Q3O)/4)*10
                
    scoreNbWB = board._nbWHITE + board._BLACK
            
    return scoreLiberty + scoreUnion + scoreEdge + scoreNbWB*30 + scoreEuler
    



b = Goban.Board()
p = b._BLACK
#for i in range(20):
#    moves = b.legal_moves()
#    move = choice(moves)
#    b.push(move)
#b.push(b.name_to_flat('D6'))
#b.push(b.name_to_flat('D5'))
#b.push(b.name_to_flat('D7'))
#b.push(b.name_to_flat('A1'))
#b.push(b.name_to_flat('D8'))
#b.push(b.name_to_flat('C3'))
#b.prettyPrint()
#print(evaluate(b,p))
#print(b._empties)


