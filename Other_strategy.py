from heuristics import *
import time
from random import *


bitstringlen = 64
squares = 81
pieces = 2
transp_table = [[None]*4]*8192 #2puissance 13
pos = 0
counter = 0
it = 0
Opening_Black = [Goban.Board.flatten(Goban.Board.name_to_coord(m)) for m in  ["G4", "D3",  "G7",  "G5",
                                                                              "D4",  "C5", "D6",
                                                                              "B6", "B4", "B7",  "H8",
                                                                              "H6",  "C4", "C2",
                                                                              "D1", "E2", "J7", "G2",
                                                                              "H3", "E2", "F1",
                                                                              "F8",  "D2",  "G9",  "B8",
                                                                              "H5","F4", "J5",
                                                                              "F9","F6", "A8","G6"]]
Opening_White = [Goban.Board.flatten(Goban.Board.name_to_coord(m)) for m in [ "E4",  "G5",  "F7",
                                                                              "F2", "F5", "D3",
                                                                              "D4", "F3", "G2",  "D2",
                                                                              "H6", "G7",  "G6",
                                                                              "E1",  "E8",  "F9",  "G8",
                                                                              "H9", "B3", "D5",
                                                                              "C5",  "C3",  "F8", "A4",
                                                                              "A3",  "H7", "J8"] ]

def rand_bit_string():
    global bitstringlen
    # Variable to store the
    # string
    key = ""
    # Loop to find the string
    # of desired length
    for i in range(bitstringlen):

        # randint function to generate
        # 0, 1 randomly and converting
        # the result into str
        temp = str(randint(0, 1))

        # Concatenatin the random 0, 1
        # to the final result
        key += temp

    return(key)

def rand_bit_string1():
    return np.random.randint(np.iinfo(np.int64).max, dtype='int64')


def init_zobrist():
    global zobrist
    zobrist = [[0]*pieces]*squares
    for i in range(squares):
        for j in range(pieces):
            zobrist[i][j] =  int(rand_bit_string())
   # return table

#init_zobrist()
#print(zobrist)
def hash(board):
    h = 0
    for i in range(squares):
        if board[i] != 0:
            j = board[i]
            h = h ^ zobrist[i][j-1]
    return h

def lookup(board,depth):
    h = hash(board)
    i = h % (2**13)
    print("lookup ",i)
    if transp_table[i][0] != h:
        return None,None
    if(depth <= transp_table[i][1]):
        return transp_table[i][2],None
    if (depth > transp_table[i][1]):
        return None,transp_table[i][3]

def store(board,depth,value,move):
    h = hash(board)
    i = h % (2**13)
    print("store ", i,move, value)
    if transp_table[i][0] is None or (transp_table[i][0] == h and depth > transp_table[i][1]):
        transp_table[i][0] = h
        transp_table[i][1] = depth
        transp_table[i][2] = value
        transp_table[i][3] = move

def alpha_beta(board,player,alpha,beta,h):
    bestmove = None
    if board.is_game_over() or h==0:
        return evaluate(board,player)
    hval,hmv = lookup(board,h)
    if hval is not None:  #if we find board with bigger depth
        return hval
    moves_list = board.generate_legal_moves()
    if hmv is not None:
        print("mv", hmv)
        #moves_list.remove(hmv)
        moves_list.insert(0,hmv)
    for m in moves_list:
        board.push(m)
        value = alpha_beta(board,player,-1*beta,-1*alpha,h-1)
        value = -1 * int(value)
        if value > alpha:
            alpha = value
            bestmove = m
        if alpha > beta :
            board.pop()
            break;
        board.pop()
    store(board,h,alpha,bestmove)
    return alpha

def ID_alpha_beta(player,alpha,beta,h,t):
    localalpha = alpha
    board = player._board
    if player._mycolor == 1:
        Opening = Opening_Black
    else:
        Opening = Opening_White
        player = board.flip(player)
    move = (None,None)
    global counter
    global it
    print(pos)
    if(counter < 10):
        while(it<len(Opening) and board[Opening[it]] != 0):
            it+=1
        if it < len(Opening):
            counter +=1
            return Opening[it]

    while(time.time()-t < 0.20):
        alpha = localalpha
        currentbestplay = None

        hval,hmv = lookup(board,h)
        moves_list = board.generate_legal_moves()
        if hmv is not None:
            print("rrtt " ,hmv)
            #moves_list.remove(hmv)
            moves_list.insert(0,hmv)
        for m in moves_list:
            board.push(m)
            value = -alpha_beta(board,board.flip(player),-1*beta,-1*alpha,h-1) #a revoir
            if value > alpha:
                alpha = value
                currentbestplay = m
            if alpha > beta :
                board.pop()
                break
            board.pop()
            print(m, alpha)
        if currentbestplay is not None and (move[1] is None or alpha > move[1]):
            move = (currentbestplay,alpha)
        store(board,h,alpha,move[0])
        h += 1
        print(time.time()-t)

    return move[0]



