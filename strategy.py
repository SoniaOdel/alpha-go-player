'''
This file contains  the alpha beta strategy with ID and openning library
'''

from heuristics import *
import time
import Goban
counter = 0
pos = 0
Opening_Black = [Goban.Board.flatten(Goban.Board.name_to_coord(m)) for m in  ["G4", "D3",  "G7",  "G5",
                                                                         "D4",  "C5", "D6",
                                                                        "B6", "B4", "B7",  "H8",
                                                                       "H6",  "C4", "C2",
                                                                        "D1", "E2", "J7", "G2",
                                                                        "H3", "E2", "F1",
                                                                        "F8",  "D2",  "G9",  "B8",
                                                                         "H5","F4", "J5",
                                                                        "F9","F6", "A8","G6"]]
Opening_White = [Goban.Board.flatten(Goban.Board.name_to_coord(m)) for m in [ "E4",  "G5",  "F7",
                                                                             "F2", "F5", "D3",
                                                                             "D4", "F3", "G2",  "D2",
                                                                              "H6", "G7",  "G6",
                                                                             "E1",  "E8",  "F9",  "G8",
                                                                              "H9", "B3", "D5",
                                                                             "C5",  "C3",  "F8", "A4",
                                                                            "A3",  "H7", "J8"] ]


def alpha_beta(board,player,alpha,beta,h):
    if board.is_game_over() or h==0:
        return evaluate(board,player)
    for m in board.generate_legal_moves():
        board.push(m)
        value = alpha_beta(board,board.flip(player),-beta,-alpha,h-1)
        value = -1 * value
        if value > alpha:
            alpha = value
        if alpha > beta :
            board.pop()
            return alpha
        board.pop()
    return alpha

def ID_alpha_beta(player,alpha,beta,h,t):
    board = player._board
    if player._mycolor == 1:
        Opening = Opening_Black
    else:
        Opening = Opening_White
        player = board.flip(player)
    move = (None,None)
    global counter
    global pos
    #use plays in opening library until 10 or stop when
    #there no more available play
    if(counter < 10):
        while(pos<len(Opening) and board[Opening[pos]] != 0):
            pos+=1
        if pos < len(Opening):
            counter +=1
            return Opening[pos]
    #then begin ID
    while time.time()-t < 0.20:
        currentbestplay = None
        for m in board.generate_legal_moves():
            board.push(m)
            value = alpha_beta(board,player,-beta,-alpha,h-1)
            value = -1 * value
            if value > alpha:
                alpha = value
                currentbestplay = m
            if alpha > beta :
                board.pop()
                break
            board.pop()
        if move[1] is None or alpha > move[1]:
            move = (currentbestplay,alpha) #remember the best play
        h += 2
    return move[0]
